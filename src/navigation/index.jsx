import React from 'react';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import DebitCardScreen from '../screens/DebitCardScreen';
import PaymentScreen from '../screens/PaymentScreen';
import CreditScreen from '../screens/CreditScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SpendingLimitScreen from '../screens/SpendingLimitScreen';
import { Feather,Fontisto,AntDesign,Entypo, FontAwesome} from '@expo/vector-icons';
import { COLORS } from '../constants/Colors';
import { Text } from 'react-native';
const Tab = createBottomTabNavigator();

export default function TabNavigation() {


    const renderTabIcon = (route, isFocused) => {
        switch (route.name) {
            case 'Home':
                return <Feather name='home' size={20} color={isFocused ? COLORS.primary : COLORS.gray4}/>
            case 'Debit':
                return<Fontisto name='mastercard' size={18} color={isFocused ? COLORS.primary : COLORS.gray4} />
            case 'Payment':
                return <AntDesign name='swap' size={25} color={isFocused ? COLORS.primary : COLORS.gray4} />
            case 'Credit':
                return <Entypo name='credit' size={20} color={isFocused ? COLORS.primary : COLORS.gray4} />
            case 'Profile':
                return <FontAwesome name='smile-o' size={22} color={isFocused ? COLORS.primary : COLORS.gray4} />
            }
    }

    const renderLabel = (route, isFocused) => {
        switch (route.name) {
            case 'Home':
                return <Text style={{color: isFocused ? COLORS.primary : COLORS.gray4}}> {route.name} </Text>
            case 'Debit':
                return <Text style={{color: isFocused ? COLORS.primary : COLORS.gray4}}>{route.name}</Text>
            case 'Payment':
                return <Text style={{color: isFocused ? COLORS.primary : COLORS.gray4}}>{route.name}</Text>
            case 'Credit':
                return <Text style={{color: isFocused ? COLORS.primary : COLORS.gray4}}>{route.name}</Text>
            case 'Profile':
                return <Text style={{color: isFocused ? COLORS.primary : COLORS.gray4}}>{route.name}</Text>

        }
    }


    const DebitStack = createNativeStackNavigator()


    function DebitStackNavigation() {
            return (
                <DebitStack.Navigator initialRouteName='Topup'>
                    <DebitStack.Screen name="Topup" component={DebitCardScreen}  />
                    <DebitStack.Screen name="Spending" component={SpendingLimitScreen} />
                </DebitStack.Navigator>
               )
    }

    return (
    <NavigationContainer>
        <Tab.Navigator screenOptions={({route}) => ({
            tabBarIcon: ({ focused }) => renderTabIcon(route, focused),
            tabBarLabel: ({ focused }) => renderLabel(route, focused),
            headerShown: false
        })}>
            <Tab.Screen name='Home' component={HomeScreen}/>
            <Tab.Screen name='Debit' component={DebitStackNavigation} />
            <Tab.Screen name='Payment' component={PaymentScreen}/>
            <Tab.Screen name='Credit' component={CreditScreen}/>
            <Tab.Screen name='Profile' component={ProfileScreen}/>
        </Tab.Navigator>
        
    </NavigationContainer>
    )
  
}
   
  