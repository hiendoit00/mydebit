const primary = '#01D167';
const primaryOpacity = 'rgba(32, 209, 103, 0.1)'
const subPrimary = 'rgb(230, 246, 238)';
const blue1 = '#0C365A';
const blue2 = '#25345F';
const gray1 = '#0000001F';
const gray2 = '#00000029';
const gray3 = '#00000014';
const gray4 = '#DDDDDD';
const gray5 = '#EEE';
const gray6 = '#22222266';
const gray7 = '#22222233';
const white = '#FFF';
const white2 = '#FFFFFF00';
const dark = '#222';
const black = '#000';
const dark40 = 'rgba(34, 34, 34, 0.4)';
const black12 = 'rgba(0, 0, 0, 0.12)';

export const COLORS = {
  primary,
  blue1,
  blue2,
  gray1,
  gray2,
  gray3,
  gray4,
  gray5,
  gray6,
  gray7,
  dark,
  white,
  white2,
  dark40,
  subPrimary,
  black,
  black12,
  primaryOpacity
};