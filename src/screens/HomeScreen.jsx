import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HomeScreen = () => {

    return ( 
        <View style={style.container}>
            <Text> HomeScreen</Text>
        </View>
    )
}
const style = StyleSheet.create({
    container:{
        flex:1,
        margin: 30
        
    }

})

export default HomeScreen