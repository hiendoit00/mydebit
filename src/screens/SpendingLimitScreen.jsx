import React, {useState} from 'react';
import { View, Text, TextInput,StyleSheet , Pressable} from 'react-native';
import { COLORS } from '../constants/Colors.js';
import {AntDesign,FontAwesome} from '@expo/vector-icons'

const SpendingLimitScreen = (props) => {
        const spendingProps = props.SpendingLimitScreen
        const [number, setNumber] = useState('')
        
            
        return (
            <View style={styles.container}>

                <View style={styles.tittle}>
                    <Text style={styles.textTittle}> 
                     SpendingLimitScreen </Text>
                </View>

                <View style={styles.currencyWarrper}>
                    <View style={styles.subTittle}>
                        <AntDesign name='warning' size={20}/> 
                        <Text style={styles.textSubTittle}> Set a weekly debit card spending limit </Text>
                    </View>

                    <View style={styles.currencyDolla}>
                        <View style={styles.currencyBackGround}>
                            <FontAwesome name='usd' size={20} color={COLORS.white} />
                        
                        </View>
                            <TextInput
                                style={styles.input}
                                onChangeText ={(val ) => {setNumber(val)}}
                                value={number.toString()}
                            />
                      
                    </View>


                    <View style={styles.boxSubNumber}>
                        <Text style={styles.textSubNumber}>
                             Here weekly means the last 7 days - not the calendar week 
                        </Text>
                    </View>

                    <View style={styles.currencyNumber}>
                        <Pressable style={styles.boxNumber} onPress={()=> setNumber('5000')}>
                            <Text style={styles.boxText}> S$ 5,000</Text>
                        </Pressable>
                        <Pressable style={styles.boxNumber} onPress={() => setNumber('10000')}>
                            <Text style={styles.boxText}> S$10,000</Text>
                        </Pressable>
                        <Pressable style={styles.boxNumber} onPress={() => setNumber('20000')}>
                            <Text style={styles.boxText}> S$20,000</Text>
                        </Pressable>
                    </View>
                </View>               
                

                <View style={styles.saveBottom}>
                    <Pressable style={[styles.saveBoxWarrper, {
                        backgroundColor: number.length ?   COLORS.primary : COLORS.gray5
                    }]} onPress={() => compareCurrency()}>
                        <Text style={styles.textSave}>
                            Save  
                        </Text>
                    </Pressable>
                </View>
                
            </View>
        )


}
const styles = StyleSheet.create ({
    container: {
        flex: 1,
        margin: 1,
        paddingTop: 30,
        backgroundColor: COLORS.blue1
    },

    input :{
        width:'90%',
        color:COLORS.black,
        fontSize: 18,
        paddingLeft: 10
    },
    boxText: {
        color: COLORS.primary,
        fontWeight: '600'
    }, 
    tittle:{
        flex: 1,
        margin: 5,
        padding: 5
    },
    currencyWarrper:{
        flex: 2,
        backgroundColor: COLORS.white,
        borderTopRightRadius: 25,
        borderTopStartRadius: 25,
        padding: 20
    },
    saveBottom:{
        flex: 3,
        flexDirection: 'column-reverse',
        backgroundColor: COLORS.white,
        paddingBottom: 10     
    },
    currencyNumber:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // marginLeft:5,
   
        // padding:1,
    },
    boxNumber:{
        width: 100,
        height: 40,
        padding: 10,
        marginRight: 10,
        backgroundColor:COLORS.primaryOpacity
    }, 
    currencyDolla:{
      flexDirection:'row'  
    },
    currencyBackGround:{
        padding: 7,
        backgroundColor: COLORS.primary,
        flexDirection: 'row',
        width: 27,
    },
    saveBoxWarrper:{
        width: 300,
        height: 56,
        marginLeft: 60,
        padding: 10,
        justifyContent:'center',
        flexDirection: 'column-reverse',
        backgroundColor:COLORS.primary,
        borderRadius: 25

    },
    boxSubNumber:{
        marginTop: 20,
        paddingBottom: 20,
    },
    textSave:{
        textAlign: 'center',
        fontSize: 16,
        color:COLORS.white
    },
    textTittle:{
        fontSize: 24,
        color: COLORS.white,
    },
    subTittle:{
        flexDirection: 'row',
        paddingBottom: 25
    },
    textSubTittle:{
        fontSize: 16,
        color:COLORS.black
    },
    textSubNumber:{
        fontSize: 13,
        fontWeight: '300',
        color: COLORS.blue2,
    }






})
export default SpendingLimitScreen