import React,{useState} from 'react';
import { View, Text, StyleSheet, Pressable } from 'react-native';
import { COLORS } from '../constants/Colors.js';
import { mockDebit } from '../data-services';
import {AntDesign, FontAwesome} from '@expo/vector-icons'



const debitNumber = ['5', '6', '4', '7', '3', '4', '1', '1', '2', '4', '1', '3', '2', '0', '2', '0']

const DebitCardScreen = ({ navigation }) => {

    const [showNumber, setShowNumber] = useState(true)

    const renderHide = () => {
        return (
            <View style={styles.hideWarraper}>
                <Pressable style={styles.hideStyle} onPress={() => {
                   setShowNumber(!showNumber)
                }}>
                    <View style={styles.iconHide}>
                        <FontAwesome name='eye-slash' size={20} color={COLORS.primary} />
                    </View>
                        <Text style={styles.textHide}> Hide card number </Text>
                </Pressable>
            </View>
            
        )
    }

    console.log('IS SHOW', showNumber);

    const renderDebitNumber = () => {
        return debitNumber.map((item, index) => (
            <View key={index}>
                <Text style={styles.textSeriCard}>{showNumber ? item : '*'}</Text>
            </View>
        ))
    }

    const renderCard = () => {
        return (
            <View style={styles.cardWarrper}>
                    {/* <Text>CARD</Text> */}
                <View style={styles.tittleIcon}>
                        <Text style={styles.textTittelIcon} >aspie</Text>
                    <View style={styles.icon}>
                        <AntDesign name='bank' size={22} color={COLORS.white} />
                    </View>
                        
                </View>


                <View style={styles.nameCard}>
                    <Text style={styles.textNameCard}> Mark Henry</Text>
                </View>


                <View style={styles.seriCard}> 
                        {renderDebitNumber()}
                       
                </View>


                <View style={styles.dateCard}>
                            <Text style={styles.textDateCard}>Thru:12/20</Text>
                        <View style={styles.dateCardOne}>
                            <Text style={styles.textDateCard}>CVV:{showNumber ? '456' : '***'}</Text>
                        </View>
                </View>


                <View style={styles.visaCard}>
                        <Text style={styles.textVisa}>Visa</Text>
                </View>
            </View>
        )
    }

    const renderMockDebit = () => {
        return mockDebit.menu.map((item) => (
            <Pressable key={item.id} style={styles.itemContainer}
             onPress={() => {
                 if (item.id === 1){
                 navigation.navigate('Spending')
                 }
             }}>
                <View style={styles.styleTitle}>
                    <Text style={styles.textTittle}>{item.title}</Text>
                </View>
                <View style={styles.styleSubTittle}>
                    <Text style={styles.textSubTitle}>{item.subTitle}</Text>
                </View>
            </Pressable>
            )
        )
    }

    return (
        <View style={styles.container}>
            {renderHide()}
            {renderCard()}
            {renderMockDebit()}
        </View>
    )
}

const styles = StyleSheet.create ({
        container:{
            flex: 1,
            margin: 10,
            // backgroundColor: COLORS.blue1
        },

        itemContainer: {
            marginTop: 15,
        },  
        styleTitle:{
            marginLeft:10,
            marginTop: 10,
        },
        textTittle:{
            color:COLORS.blue2,
            fontSize: 14,
            fontWeight: '400'
        },
        styleSubTittle:{
            marginTop: 4 ,
            marginLeft: 10,
        },
        textSubTitle:{
            fontSize: 13,
            fontWeight:'100',
            color:COLORS.dark

        },
        cardWarrper:{
            // flex:1,
            marginLeft: 10,
            padding: 10,
            backgroundColor: COLORS.primary,
            width: 366,
            height: 220,
            borderRadius: 10
        },
        tittleIcon:{
            // flex:2,
            flexDirection:'row-reverse',
            margin: 5,
            padding:10
        },
        nameCard:{
            marginLeft: 10,
            padding:5,
        },
        seriCard:{
            flexDirection: 'row',
            marginLeft: 10,
            padding: 10,
        },
        dateCard:{
            flexDirection:'row',
            marginLeft: 18,
            paddingTop: 10,
        },
        dateCardOne:{
            paddingLeft:40,
        },
        visaCard:{
            marginRight: 5,
            // padding:10
        },
        icon:{
            paddingRight:5,
            flexDirection:"row-reverse",
        },       
        textTittelIcon:{
            fontSize: 18,
            textAlign:'right',
            color:COLORS.white
        },
        textNameCard:{
            fontSize: 22,
            color:COLORS.white,
            fontWeight:'800'
        },
        textSeriCard:{
            fontSize: 14,
            color:COLORS.white,
        },
        textDateCard:{
            fontSize: 13,
            color:COLORS.white,
        },
        textVisa:{
            fontSize: 30,
            color:COLORS.white,
            textAlign:'right'
        },
        hideWarraper:{
        
            flexDirection: 'row-reverse',
        
           
        },
        hideStyle:{
            width: 170,
            height: 44,
            backgroundColor: COLORS.white,
            flexDirection:'row',
            borderRadius: 10,
            marginRight:20
            
           
        },
        textHide:{
            textAlign :'right',
            paddingLeft: 5,
            paddingTop: 12,
            color:COLORS.primary

        },
        iconHide:{
            paddingTop:10,
            paddingLeft:10

        }



    })
export default DebitCardScreen