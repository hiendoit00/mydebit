export const mockDebit = {
    personalFinanceStatus: {
      availableBalance: 3000,
      spent: 1200,
    },
  
    cardInfo: {
      name: 'Mark Henry',
      cardNumber: ['5647', '3411', '2413', '2020'],
      expireTime: '12/20',
      cvv: '456',
    },
    menu: [
      {
        id: 0,
        title: 'Top-up account',
        subTitle: 'Deposit money to your account to use with card',
      },
      {
        id: 1,
        title: 'Weekly spending limit',
        subTitle: "You haven't set any spending limit on card",
        isToggle: true,
      },
      {
        id: 2,
        title: 'Freeze card',
        subTitle: 'Your debit card is currently active',
        isToggle: true,
      },
      {
        id: 3,
        title: 'Get a new card',
        subTitle: 'This deactivates your current debit card',
      },
      {
        id: 4,
        title: 'Deactivated cards',
        subTitle: 'Your previously deactivated cards',
      },
    ],
    limitRange: [
      {
        key: '5000',
        value: '5,000',
      },
      {
        key: '10000',
        value: '10,000',
      },
      {
        key: '20000',
        value: '20,000',
      },
    ],
  };